import re
import sys
import pymongo as pm
import configparser
from collections import Counter

import relevancer as rlv

rt_re = re.compile(r'\brt @', re.IGNORECASE)
source_re = re.compile(r'>(.+)<')

def generate_metadata(collname, which_db):

	if(which_db == "localdb"):
		dbfile = "data/localdb.ini"

	elif(which_db == "mongolab_ebasar"):
		dbfile = "data/ebasar_rel.ini"

	rlvdb, coll = rlv.connect_mongodb(configfile=dbfile, coll_name=collname)
	rlvdb, metacoll = rlv.connect_mongodb(configfile=dbfile, coll_name="MetaDataLists")


	lang_cntr = Counter()
	hashtag_cntr = Counter()
	user_cntr = Counter()
	source_cntr = Counter()
	location_cntr = Counter()
	rt_num = 0
	link_num = 0

	numoftweet = coll.count()

	for t in coll.find():
    
		lang_cntr[t["lang"]] += 1

		if "retweeted_status" in t:
			rt_num += 1
			t["is_retweet"] = True
		else:
			if re.search(rt_re, t['text']):
				rt_num += 1
            
		if t['entities']['urls']:
			link_num += 1

		for i in t['entities']['hashtags']:
			hashtag_cntr[i['text']] += 1
        
		user_cntr[t['user']['screen_name']] += 1
    
		source_cntr[t['source']] += 1        
    


	langs = []
	for value, count in lang_cntr.items():
		langs.append({"lang" : value, "numtweet" : count})


	users = []
	exampletweets = []
	for value, count in user_cntr.most_common()[:10]:
		for t in coll.find({'user.screen_name': value})[:5]:
			exampletweets.append(t['text'])
		users.append({"username" : value, "numtweet" : count, "exampletweets" : exampletweets})
		exampletweets = []


	hashtags = []
	exampletweets = []
	for value, count in hashtag_cntr.most_common()[:10]:
		for t in coll.find({'entities.hashtags.text': value})[:5]:
			exampletweets.append(t['text'])
		hashtags.append({"hashtag" : value, "numtweet" : count, "exampletweets" : exampletweets})
		exampletweets = []


	sources = []
	for value, count in source_cntr.most_common():
		if count < 100:
			break
		name = re.findall(source_re, value)
		sources.append({"sourcename" : name[0], "source" : value, "numtweet" : count})
  


	metadata = {}
	metadata['collname'] = 'alldata'
	metadata['numoftweets'] = numoftweet
	metadata['numofretweets'] = rt_num
	metadata['numoflinks'] = link_num
	metadata['langs'] = langs
	metadata['users'] = users
	metadata['hashtags'] = hashtags
	metadata['sources'] = sources

	metacoll.insert(metadata)


if __name__ == "__main__":

	
	collname = sys.argv[1]

	which_db = sys.argv[2]

	generate_metadata(collname, which_db)

     
