# Python 
import re
import os
import sys
import traceback
import pymongo
import logging
import smtplib 
import requests
import subprocess
import configparser
import pandas as pd
import math as m
import numpy as np

from sklearn.naive_bayes import MultinomialNB, GaussianNB
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn import metrics
from sklearn import cross_validation as cv
from sklearn import grid_search as gs
from sklearn.metrics import classification_report

import pickle

# Our Own Sources
sys.path.append('../') # adds 'Relevancer' folder to PYTHONPATH to find relevancer.py etc.
import relevancer as rlv

def send_mail(sbj, msg, to):

	configauth = configparser.ConfigParser()
	configauth.read("data/auth.ini")

	if(to=="admin"):
		toaddrs = configauth.get('mail', 'admin')
	else:
		toaddrs  = to

	#Mail Auth;
	fromaddr = configauth.get('mail', 'fromaddr')
	username = configauth.get('mail', 'user_name') 
	password = configauth.get('mail', 'password')
	subject = sbj
	message = 'Subject: ' + subject + '\n\n' + msg
	server = smtplib.SMTP('smtp.gmail.com:587')  
	server.starttls()  
	server.login(username,password)  
	server.sendmail(fromaddr, toaddrs, message)  
	server.quit() 


def get_evaluated_classifier(tweets_as_text_label_df, my_token_pattern): 
    
	freqcutoff = int(m.log(len(tweets_as_text_label_df))/2)

	word_vectorizer = TfidfVectorizer(ngram_range=(1, 3), lowercase=False, norm='l2', min_df=freqcutoff, token_pattern=my_token_pattern, sublinear_tf=True)
	fitted_data = word_vectorizer.fit_transform(tweets_as_text_label_df.text.values)
    
	x = fitted_data
    
	X_train, X_test, y_train, y_test = cv.train_test_split(fitted_data, tweets_as_text_label_df.label.values, test_size=0.15, random_state=0)
    
    # Grid Search
	kf_total = cv.StratifiedKFold(y_train, n_folds=10, shuffle=True)
	nb_gs = gs.GridSearchCV(estimator=MultinomialNB(), param_grid=dict(alpha=np.linspace(0,2,20)), n_jobs=6) # , n_jobs=6

	[nb_gs.fit(X_train[train],y_train[train]).score(X_train[test],y_train[test]) for train, test in kf_total]

    # Run with best alpha
	MNB = MultinomialNB(alpha=nb_gs.best_estimator_.alpha)

	MNB.fit(X_train, y_train)
    
	predicted = MNB.predict(X_test)
	expected = y_test

	conf_m = metrics.confusion_matrix(expected, predicted, labels=MNB.classes_)
	clf_report = classification_report(expected, predicted, target_names=MNB.classes_)
	vect_and_classifier = {'vectorizer': word_vectorizer, 'classifier': MNB, 'conf_mtrx':conf_m, 'report':clf_report}

	
	return vect_and_classifier, conf_m, clf_report


# Relevancer Clustering; 

def generate_classifier(collname, APIURL, useremail):

	sbj = 'Generating Classifier - Relevancer'

	msg = '\nClassifier generation is started' +\
			'\n\nThe collection name: ' + collname +\
			'\n\nUser Email : ' + useremail +\
			'\nYou will get another email when the generation is done.'

	send_mail(sbj, msg, useremail)
	send_mail(sbj, msg, "admin")

	my_token_pattern = r"[-+$€£]?\d+(?:[.,/-]\d+)*%?|\w+(?:[-&]\w+)*|[#@]?\w+\b|[\U00010000-\U0010ffff\U0001F300-\U0001F64F\U0001F680-\U0001F6FF\u2600-\u26FF\u2700-\u27BF]|['-.:()\[\],;?!*]{2,4}"

	vect_and_classifier = None

	try:
		gen_data = requests.get(APIURL + 'getdata/relevancerclusters/' + collname).json()['documents']

		label_tweets_dict = {}

		for gc in gen_data:
			if "label" in gc:
		
				if gc["label"] not in label_tweets_dict:
					label_tweets_dict[gc["label"]] = []
				label_tweets_dict[gc["label"]] += [tt[2] for tt in gc['ctweettuplelist']]
		
		if len(label_tweets_dict.keys())<2:
			raise Exception("There is not enough label types for creating a classifier. Number of label types:"+ str(len(label_tweets_dict.keys())))
			
		print("available labels:", label_tweets_dict.keys())

		label_tweet_tuple_list = []
		for k,v in label_tweets_dict.items():
				label_tweet_tuple_list += [(k,vv) for vv in v]

		tweets_df = pd.DataFrame({'label':[lab_tw[0] for lab_tw in label_tweet_tuple_list],'text':[lab_tw[1] for lab_tw in label_tweet_tuple_list]})

		vect_and_classifier, conf_m, clf_report = get_evaluated_classifier(tweets_df, my_token_pattern)

	except:
		sbj = "Classifier Generation Failed - Relevancer"

		msg = '\n\nClassifier generation was failed' +\
			'\n\nException Error; \n\n' + str(traceback.format_exc()) +\
			'\n\nThe collection name: ' + collname +\
			'\n\nThis mail was sent to : ' + useremail
		traceback.print_exc()
		send_mail(sbj, msg, useremail)
		send_mail(sbj, msg, "admin")


	if(vect_and_classifier is None):
		print('Classifier is not generated. There is an error. Please inform the admins.')
	else:
		with open('data/classifiers/' + collname + '_classifier.pickle', 'wb') as f:
			 pickle.dump(vect_and_classifier, f, pickle.HIGHEST_PROTOCOL)
		with open('data/classifiers/' + collname + 'vect_and_classifier.txt', 'w') as f:
			 f.write(str(vect_and_classifier))
		with open('data/classifiers/' + collname + 'conf_m.txt', 'w') as f:
			 f.write(str(conf_m))
		with open('data/classifiers/' + collname + 'clf_report.txt', 'w') as f:
			 f.write(str(clf_report))

	# Send Email to user and admin when all is done
	sbj = "Classifier was Generated - Relevancer"
	msg = '\n\nClassifier was generated' +\
			'\n\nThe collection name: ' + collname +\
			'\n\nPlease go to relevancer.science.ru.nl/datasets/' + collname + '/listclassifiers.' +\
			'\n\nThis mail was sent to : ' + useremail

	send_mail(sbj, msg, useremail)
	send_mail(sbj, msg, "admin")

