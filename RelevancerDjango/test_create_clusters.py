import django_relevancer_clustering as drc
from multiprocessing import Process

if __name__ == "__main__":
	# first tests, works
	#p = Process(target=drc.auto_cluster, args=('floodSampleEn10k', '57f548aa385ee46c42dfcec5', 'http://127.0.0.1:5007/'))

	# after key error: 'date', this worked without any error.
	#p = Process(target=drc.auto_cluster, args=('floodSampleEn50k', '57fde914385ee46c42dfcf04', 'http://127.0.0.1:5007/'))

	# is 'date' error here? 57fb84eb385ee46c42dfcf03
	p = Process(target=drc.auto_cluster, args=('floodSampleEn100k', '57fb84eb385ee46c42dfcf03', 'http://127.0.0.1:5007/'))

	p.start()
	print("done")
