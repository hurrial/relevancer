
from django.db import models

from mongoengine import * #DynamicDocument, ListField, StringField, IntField

# Collections to show in 'how does it work';

class testcl(DynamicDocument): # raw tweet data

	meta = {'collection' : 'testcl'}
	text = StringField()

class rt_eliminated(DynamicDocument): # retweets are eliminated 

	meta = {'collection' : 'rt_eliminated'}
	text = StringField()

class duplicates_eliminated(DynamicDocument): # duplicates are eliminated 

	meta = {'collection' : 'duplicates_eliminated'}
	text = StringField()

