# Django
from django.shortcuts import render
from django.views.generic import View
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse
# from django.core.urlresolvers import reverse
from django.core.files.storage import FileSystemStorage

from django.conf import settings

# Python.
import re
import os
import sys
import json
import pickle
import time
import smtplib # mail
import requests
import logging
import configparser
from datetime import datetime
# from bson.json_util import dumps
#import subprocess
# from itertools import chain
from multiprocessing import Process

import tweepy
import numpy as np
# from sklearn.metrics import classification_report
from sklearn.metrics import precision_recall_fscore_support
import pandas as pd

# DB / models
# import mongoengine
from main.models import * # Clusters, CollectionList (Have to import everything(with star) because the models can be added dynamically)
from mongoengine.base.common import get_document

# Our Own Sources
sys.path.append('../') # adds 'Relevancer' folder to PYTHONPATH
import django_relevancer_clustering as drc
import generate_classifier as gc
import json_tweets_parser as jtp

HOSTNAME = os.uname()[1]

config = configparser.ConfigParser()

config.read("data/auth.ini")

logging.basicConfig(
    format='%(asctime)s, %(levelname)s: %(message)s',
    filename='data/relevancer.log',
    datefmt='%d-%m-%Y, %H:%M',
    level=logging.INFO)

logging.info('Log started')


def send_mail(sbj, msg, to):

    configauth = configparser.ConfigParser()
    configauth.read("data/auth.ini")

    toaddrs = configauth.get('mail', to)

    #Mail Auth;
    fromaddr = configauth.get('mail', 'fromaddr')
    username = configauth.get('mail', 'user_name')
    password = configauth.get('mail', 'password')
    subject = sbj
    message = 'Subject: ' + subject + '\n\n' + msg
    server = smtplib.SMTP('smtp.gmail.com:587')
    server.starttls()
    server.login(username, password)
    server.sendmail(fromaddr, toaddrs, message)
    server.quit()


############################## FUNCTIONS FOR VIEWS ##############################


def get_step_data(collname, num, page=None):

    model = get_document(collname)

    tweets = []

    if page == "Cluster_Them":
        for item in model.objects[5:num+5]:
            for i in item["ctweettuplelist"][:10]:
                tweets.append(i[2])
            tweets.append("------------------")

    else:
        for item in model.objects[5:num+5]:
            tweets.append(item["text"])

    length = model.objects.count()

    return tweets, length


def get_counts(dbname, collname, opt=''):

    req = requests.get(settings.APIURL + 'count/' + dbname + '/'+ collname + opt)
    count = req.json()['collection_size']

    return count

def backup_json(collname):

    currDate = datetime.now().strftime("%y%m%d-%H:%M")

    filename = collname + "_" + currDate + ".json"

    documents = requests.get(settings.APIURL + 'getdata/relevancerclusters/' + collname ).json()

    with open('data/backups/' + filename, 'w+') as f:
        json.dump(documents, f)

    logging.info('BACKUP : Backup to ' + filename)

    return 0

def loadbackup(filename):

    collname = re.findall('(.*)_\d{6}-\d\d:\d\d.json', filename)[0]

    backup_json(collname)

    with open("data/backups/" + filename) as f:
        docs_json = json.load(f)

    removereq = requests.get(settings.APIURL + 'remove/relevancerclusters/' + collname + '/collection')

    insertreq = requests.post(settings.APIURL + 'insert/relevancerclusters/' + collname, json=json.dumps(docs_json))

    return 0

def get_client_ip(request):
    ip = request.META.get('HTTP_CF_CONNECTING_IP')
    if ip is None:
        ip = request.META.get('REMOTE_ADDR')
    return ip


##################### FUNCTIONS SERVED BY ITSELF #####################

def handler404(request):
    response = render_to_response('404.html', {}, context_instance=RequestContext(request))
    response.status_code = 404
    return response

def handler500(request):
    response = render_to_response('500.html', {}, context_instance=RequestContext(request))
    response.status_code = 500
    return response


def downloadfile(request, opt, filename):

    print("opt and filename:", opt, filename)

    if opt == 'classifiers':
        with open("data/" + opt + "/" + filename, 'rb') as f:
            data = pickle.load(f)

        response = HttpResponse(pickle.dumps(data, protocol=pickle.HIGHEST_PROTOCOL), content_type='application/octet-stream')
        response['Content-Disposition'] = 'attachment; filename="' + filename + '"'

    elif opt == 'reports':
        with open("data/classifiers/" + filename, 'rb') as f:
            data = pickle.load(f)
            data_str = str(data["vectorizer"]) + "<br><br>"  +str(data["classifier"]) + "<br><br>" + str(data["conf_mtrx"]) + "<br><br>" + str(data["report"])
            data_str=data_str.replace(' ', "&nbsp;").replace('\n', '<br>')

        response = HttpResponse(data_str)

    elif opt == "allfilter":
        filtername=filename.split("_")[-3]
        filtername_document=requests.get(settings.APIURL + 'getdata/relevancer/QueryList/filtername/' + filtername).json()

        with open("data/" + "classifiers" + "/" + filename, 'rb') as f:
            filtername_document["classifier"] = pickle.load(f)

    #  dict_keys(['tweets_urls', 'tweets_photos', 'tweets_nophotosurls',])

        filtername_document = {k:v for k,v in filtername_document.items() if k in ["filtername", "classifier", "keyword",\
                "hashtaglist_nophotosurls", "hashtaglist_photos", "hashtaglist_urls", "userlist_nophotosurls", \
                "userlist_photos", "userlist_urls", 'usersurname', 'username']}

        response = HttpResponse(pickle.dumps(filtername_document, protocol=pickle.HIGHEST_PROTOCOL), content_type='application/octet-stream')
        response['Content-Disposition'] = 'attachment; filename="' + filename + '"'

    else:
        with open("data/" + opt + "/" + filename, 'r') as f:
            data = json.load(f)
        response = HttpResponse(data, content_type='application/json')
        response['Content-Disposition'] = 'attachment; filename="' + filename + '"'

    return response


############################## VIEWS ##############################

class Home(View):

    def get(self, request):

        return render(request, 'base.html', {

        })


class Datasets(View):

    def get(self, request):

        db = 'relevancerclusters'

        req = requests.get(settings.APIURL + 'dbinfo/' + db)

        collectionlist = req.json()['collections']

        mycol = {}

        for coll in collectionlist :

            print(coll)

            if coll.endswith("_urls") and not coll.endswith("_nophotosurls"):

                if coll[:-5] not in mycol.keys():
                    mycol[coll[:-5]]= {}
                mycol[coll[:-5]]["urls"] = {"clusters":get_counts(db,coll), "not_labeled":get_counts(db,coll,'/unlabeled') , "labeled":get_counts(db,coll,'/labeled') }

            elif coll.endswith("_photos"):

                if coll[:-7] not in mycol.keys():
                    mycol[coll[:-7]] = {}
                mycol[coll[:-7]]["photos"] = {"clusters":get_counts(db,coll), "not_labeled":get_counts(db,coll,'/unlabeled') , "labeled":get_counts(db,coll,'/labeled') }

            elif coll.endswith("_nophotosurls"):

                if coll[:-13] not in mycol.keys():
                    mycol[coll[:-13]] = {}
                mycol[coll[:-13]]["nophotosurls"] = {"clusters":get_counts(db,coll), "not_labeled":get_counts(db,coll,'/unlabeled') , "labeled":get_counts(db,coll,'/labeled') }

            else:

                mycol[coll] = {}
                mycol[coll]= {"clusters":get_counts(db,coll), "not_labeled":get_counts(db,coll,'/unlabeled') , "labeled":get_counts(db,coll,'/labeled') }


        return render(request, 'datasets.html', {
                'mycol':mycol,
        })

class ListClassifiers(View):

    def get(self, request, collname):

        filelist = []

        folder = "data/classifiers"
        os.makedirs(folder, exist_ok=True)

        for file in os.listdir(folder):
            if file.startswith(collname) and file.endswith(".pickle"):
                filelist.append(file)

        filelist.sort(reverse=True)

        return render(request, 'classifiers.html', {
            'collname' : collname,
            'filelist' : filelist,
        })

class EvaluationReport(View):

    def get(self, request, collname):

        filelist = []

        folder = "data/classifiers"
        os.makedirs(folder, exist_ok=True)

        for file in os.listdir(folder):
            if file.endswith(".pickle"):
                filelist.append(file)

        filelist.sort(reverse=True)

        return render(request, 'evaluationreport.html', {
            'collname' : collname,
            'filelist' : filelist,
        })

class EvaluationYourself(View):

    def get(self, request, filename):

        filtername = filename.split("_")[-3]
        filtername_document=requests.get(settings.APIURL + 'getdata/relevancer/QueryList/filtername/' + filtername).json()

        #print("filtername_document:", filtername_document.keys())

        used_keyword = filtername_document["keyword"]

        excluded_htag = filtername_document["hashtaglist_nophotosurls"] + filtername_document["hashtaglist_photos"] + filtername_document["hashtaglist_urls"]
        excluded_user = filtername_document["userlist_nophotosurls"] + filtername_document["userlist_photos"] + filtername_document["userlist_urls"]

        return render(request, 'evaluationyourself.html', {
            'filename': filename,
            'used_keyword': used_keyword,
            'excluded_htag': excluded_htag,
            'excluded_user': excluded_user,
        })

    def post(self, request, filename):

        filtername=filename.split("_")[-3]
        filtername_document=requests.get(settings.APIURL + 'getdata/relevancer/QueryList/filtername/' + filtername).json()

        used_keyword = filtername_document["keyword"]

        excluded_htag = filtername_document["hashtaglist_nophotosurls"] + filtername_document["hashtaglist_photos"] + filtername_document["hashtaglist_urls"]
        excluded_user = filtername_document["userlist_nophotosurls"] + filtername_document["userlist_photos"] + filtername_document["userlist_urls"]

        excluded_htag_set_no_ph_u = set(filtername_document["hashtaglist_nophotosurls"])
        excluded_htag_set_u = set(filtername_document["hashtaglist_urls"])
        excluded_htag_set_ph = set(filtername_document["hashtaglist_photos"])

        excluded_user_set_no_ph_u = set(filtername_document["userlist_nophotosurls"])
        excluded_user_set_u = set(filtername_document["userlist_urls"])
        excluded_user_set_ph = set(filtername_document["userlist_photos"])


        if "evaluate" in request.POST:
            tweet = request.POST['taname']
            tweet_list=tweet.split("\r\n")

            contains_excluded_hashtag = [len(set(re.findall(r"#\w+",t)) & excluded_htag_set_no_ph_u)>0 for t in tweet_list]
            contains_excluded_user = [len(set(re.findall(r"\w+",t)) & excluded_user_set_no_ph_u)>0 for t in tweet_list]

            folder = "data/classifiers"
            os.makedirs(folder, exist_ok=True)

            with open(folder + "/"+ filename, 'rb') as f:
                    vect_and_evaluated_classifier_nv_tst = pickle.load(f)

            mnb_classifier = vect_and_evaluated_classifier_nv_tst["classifier"]
            vectorizer = vect_and_evaluated_classifier_nv_tst["vectorizer"]

            #print("vectorizer.get_feature_names():", vectorizer.get_feature_names())

            ntw = vectorizer.transform(tweet_list) # list of tweets. It can be just one tweet in a list as well.

            my_predictions = mnb_classifier.predict(ntw) # predicted labels are in order of the input list
            class_list=mnb_classifier.classes_
            my_predictions[np.diff(ntw.indptr)==0]="1"
            my_predictions[np.array(contains_excluded_hashtag)]="2"
            my_predictions[np.array(contains_excluded_user)] = "3"

            no_pred_codes = {"1":"Feature mismatch", "2":"Excluded hashtag", "3":"Excluded user and maybe excluded hashtag too"}

            my_predictions = [no_pred_codes[p] if p in no_pred_codes else p for p in list(my_predictions)]

            results = [(t,p) for t,p in zip(tweet_list, my_predictions)]

            return render(request, 'evaluationyourself.html', {
                'filename' : filename,
                'used_keyword': used_keyword,
                'excluded_htag': excluded_htag,
                'excluded_user': excluded_user,
                'after_evaluate': results,
                'class_list': class_list,
            })

        elif "evaluaterealdata" in request.POST:
            config = configparser.ConfigParser()
            config.read('data/auth.ini')

            auth = tweepy.OAuthHandler(config['twitterapp']['consumer_key'], config['twitterapp']['consumer_secret'])
            auth.set_access_token(config['twitterapp']['access_token'], config['twitterapp']['access_secret'])
            api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

            twitterapp_list = []
            corresponding_htag = []
            corresponding_user = []

            tweet_count=0
            for qt in tweepy.Cursor(api.search, q=filtername_document["keyword"]).items(): # flood, floods, landslide, landslides #  lang='en'
                t = qt._json

                if ("urls" in t["entities"]) and ("media" in t["entities"]) and t["entities"]["urls"] and t["entities"]["media"]:
                    corresponding_htag.append(excluded_htag_set_u|excluded_htag_set_ph)
                    corresponding_user.append(excluded_user_set_u|excluded_user_set_ph)
                elif ("urls" in t["entities"]) and t["entities"]["urls"]:
                    corresponding_htag.append(excluded_htag_set_u)
                    corresponding_user.append(excluded_user_set_u)
                elif ("media" in t["entities"]) and t["entities"]["media"]:
                    corresponding_htag.append(excluded_htag_set_ph)
                    corresponding_user.append(excluded_user_set_ph)
                else:
                    corresponding_htag.append(excluded_htag_set_no_ph_u)
                    corresponding_user.append(excluded_user_set_no_ph_u)

                tweet_count+=1
                if tweet_count==50:
                    break

                twitterapp_list.append(t)


            contains_excluded_hashtag = [len(set(re.findall(r"#\w+",t["text"])) & exc_htag)>0 for exc_htag, t in zip(corresponding_htag, twitterapp_list)]
            contains_excluded_user = [t["user"]["screen_name"] in exc_user for exc_user, t in zip(corresponding_user, twitterapp_list)]

            folder = "data/classifiers"
            os.makedirs(folder, exist_ok=True)

            with open(folder + "/"+ filename, 'rb') as f:
                vect_and_evaluated_classifier_nv_tst = pickle.load(f)

            mnb_classifier = vect_and_evaluated_classifier_nv_tst["classifier"]
            vectorizer = vect_and_evaluated_classifier_nv_tst["vectorizer"]

            #print("vectorizer.get_feature_names():", ",".join(.vectorizer.get_feature_names()))

            tweet_text_list = [t["text"] for t in twitterapp_list]

            ntw = vectorizer.transform(tweet_text_list) # list of tweets. It can be just one tweet in a list as well.

            my_predictions = mnb_classifier.predict(ntw) # predicted labels are in order of the input list
            class_list=mnb_classifier.classes_
            my_predictions[np.diff(ntw.indptr)==0] = "1"
            my_predictions[np.array(contains_excluded_hashtag)] = "2"
            my_predictions[np.array(contains_excluded_user)] = "3"

            no_pred_codes = {"1":"Feature mismatch", "2":"Excluded hashtag", "3":"Excluded user and maybe excluded hashtag too"}

            my_predictions = [no_pred_codes[p] if p in no_pred_codes else p for p in list(my_predictions)]

            results = [(t,p) for t,p in zip(tweet_text_list, my_predictions)]
            print("results:", results[:5])

            return render(request, 'evaluationyourself.html', {
                'filename' : filename,
                'used_keyword': used_keyword,
                'excluded_htag': excluded_htag,
                'excluded_user': excluded_user,
                'results':results,
                'class_list':class_list,
            })

        elif "submitvalues" in request.POST:
            values = request.POST.getlist('vote')

            tweet_pre_feed_list = []

            for v in values:
                vv = v.split(":::")
                if (vv[0] == "feedback") and (vv[2]=="prediction") and (vv[4]=="text"):
                    tweet_pre_feed_list.append({vv[0]:vv[1], vv[2]:vv[3], vv[4]:":::".join(vv[5:])})
                else:
                    print("There is a problem in feedback, prediction, and text values. The raw value:", v)

            positive_feedback = 0
            print("tweet_pre_feed_list1:", tweet_pre_feed_list[:5])
            all_feedback = len(tweet_pre_feed_list)

            tweet_pre_feed_list_new = []
            for item in tweet_pre_feed_list:
                if item['prediction'] == item['feedback']:
                    positive_feedback +=1
                else :
                    tweet_pre_feed_list_new.append(item)

            # this computes precision.
            feedback_rate = round(positive_feedback / all_feedback * 100)
            tweet_pre_feed_list = tweet_pre_feed_list_new
            return render(request, 'evaluationyourself.html', {
                'filename' : filename,
                'used_keyword': used_keyword,
                'excluded_htag': excluded_htag,
                'excluded_user': excluded_user,
                'feedback_rate': feedback_rate,
                'tweet_pre_feed_list':tweet_pre_feed_list,
            })

        elif "submittweetvalues" in request.POST:
            values = request.POST.getlist('tweetvote')

            print("values:", values)

            tweet_pre_feed_list = []

            for v in values:
                vv = v.split(":::")
                if vv[0] == "feedback" and vv[2] == "prediction" and vv[4] == "text":
                    tweet_pre_feed_list.append({vv[0]:vv[1], vv[2]:vv[3], vv[4]:":::".join(vv[5:])})
                else:
                    print("There is a problem in feedback, prediction, and text values. The raw value:", v)

            print("tweet_pre_feed_list2:", tweet_pre_feed_list[:5])
            positive_feedback = 0
            all_feedback = len(tweet_pre_feed_list)

            tweet_pre_feed_list_new = []
            y_true_my = []
            y_pred_my = []
            #my_labels = []
            for item in tweet_pre_feed_list:
                y_pred_my.append(item['prediction'])
                y_true_my.append(item['feedback'])
                if item['prediction'] == item['feedback']:
                    positive_feedback += 1
                else :
                    tweet_pre_feed_list_new.append(item)

            # y_true = [0, 1, 2, 2, 2]
            # y_pred = [0, 0, 2, 2, 1]
            # target_names = ['class 0', 'class 1', 'class 2']
            #print('classification_report:', classification_report(y_true, y_pred, target_names=target_names))
            #print('precision_recall_fscore_support:', precision_recall_fscore_support(y_true, y_pred))
            p, r, f1, s = precision_recall_fscore_support(y_true_my, y_pred_my)
            df_result = pd.DataFrame({'precision':p,'recall':r,'f1-score':f1,'support':s}, index=sorted(list(set(y_true_my))))
            #print('pd.DataFrame1', df_result.to_html())
            #print('pd.DataFrame2', df_result.to_html().replace("<table", "<table align=center"))

            feedback_rate = df_result.to_html().replace("<table", "<table align=center")
            #feedback_rate = round(positive_feedback / all_feedback * 100)
            tweet_pre_feed_list = tweet_pre_feed_list_new

            return render(request, 'evaluationyourself.html', {
                'filename': filename,
                'used_keyword': used_keyword,
                'excluded_htag': excluded_htag,
                'excluded_user': excluded_user,
                'feedback_rate': feedback_rate,
                'tweet_pre_feed_list_new':tweet_pre_feed_list_new,

            })


class Classifier(View):

    def get(self, request, collname):

        action = 'classifier'
        event = 'generate a new classifier with the clusters in the  '

        return render(request, 'confirmpass.html', {
            'action': action,
            'event' : event,
            'thing' : collname,
        })

    def post(self, request, collname):

        if "confirmpass" in request.POST:

            user_pass = request.POST['user_pass']
            useremail = request.POST['useremail']

            reset_pass = config.get('dataset', 'reset_pass')

            if user_pass == reset_pass:

                client_address = get_client_ip(request)

                logging.info('CLASSIFIER : ' + client_address + ' requested to generate a classifier for ' + collname)

                p = Process(target=gc.generate_classifier, args=(collname, settings.APIURL, useremail))
                p.start()

                logging.info('CLASSIFIER : Generated the classifier for ' + collname + ', by ' + client_address)

                confirmed = True

                result = 'Classifier is successfully generated for : '

                return render(request, 'confirmpass.html', {
                    'thing' : collname,
                    'confirmed' : confirmed,
                    'result' : result,
                })

            else:

                confirmed = False
                action = 'classifier'
                event = 'generate a new classifier with the clusters in the  '
                denied_msg = "Wrong password. Please try again."

                return render(request, 'confirmpass.html', {
                    'action': action,
                    'event' : event,
                    'thing' : collname,
                    'confirmed' : confirmed,
                    'denied_msg' : denied_msg,
                })


class Backup(View):

    def get(self, request, collname):

        action = 'backup'
        event = 'create a new backup file for '

        return render(request, 'confirmpass.html', {
            'action': action,
            'event' : event,
            'thing' : collname,
        })

    def post(self, request, collname):

        if "confirmpass" in request.POST:

            user_pass = request.POST['user_pass']

            reset_pass = config.get('dataset', 'reset_pass')

            if user_pass == reset_pass:

                client_address = get_client_ip(request)

                logging.info('BACKUP : ' + client_address + ' requested to backup ' + collname)

                backup_json(collname)

                logging.info('BACKUP : Backup done with ' + collname + ', by ' + client_address)

                confirmed = True

                result = 'Backup is successfully created for : '

                return render(request, 'confirmpass.html', {
                    'thing' : collname,
                    'confirmed' : confirmed,
                    'result' : result,
                })

            else:

                confirmed = False
                action = 'backup'
                event = 'create a new backup file for '
                denied_msg = "Wrong password. Please try again."

                return render(request, 'confirmpass.html', {
                    'action': action,
                    'event' : event,
                    'thing' : collname,
                    'confirmed' : confirmed,
                    'denied_msg' : denied_msg,
                })



class ListBackups(View):

    def get(self, request, collname):

        filelist = []

        for file in os.listdir("data/backups/"):
            if file.startswith(collname):
                filelist.append(file)

        filelist.sort(reverse=True)

        return render(request, 'loadback.html', {
            'collname' : collname,
            'filelist' : filelist,
        })



class LoadBack(View):

    def get(self, request, filename):

        action = 'loadback'
        event = 'load the database backup on'

        return render(request, 'confirmpass.html', {
            'action': action,
            'event' : event,
            'thing' : filename,
        })


    def post(self, request, filename):

        if "confirmpass" in request.POST:

            user_pass = request.POST['user_pass']

            reset_pass = config.get('dataset', 'reset_pass')

            if(user_pass == reset_pass):

                client_address = get_client_ip(request)

                logging.info('LOAD : ' + client_address + ' requested to load ' + filename)

                loadbackup(filename)

                logging.info('LOAD : Load done with ' + filename + ', by ' + client_address)

                sbj = "Backup Reload"
                msg = sbj + '\n\nFile loaded : ' + filename + '\nIP address : ' + client_address + "\n\nThis mail sent to : ebasar"

                if HOSTNAME[:9] == "applejack":
                    msg2 = msg + ", admin"
                    send_mail(sbj, msg2, "admin")
                    send_mail(sbj, msg2, "ebasar")
                else:
                    send_mail(sbj, msg, "ebasar")

                confirmed = True

                result = 'Following backup is successfully loaded back : '

                return render(request, 'confirmpass.html', {
                    'thing' : filename,
                    'confirmed' : confirmed,
                    'result' : result,
                })

            else:

                confirmed = False
                action = 'loadback'
                event = 'load the database backup on '
                denied_msg = "Wrong password. Please try again."

                return render(request, 'confirmpass.html', {
                    'action': action,
                    'event' : event,
                    'thing' : filename,
                    'confirmed' : confirmed,
                    'denied_msg' : denied_msg,
                })



class Download(View):

    def get(self, request, opt, filename):

        action = 'download'
        event = 'download '

        return render(request, 'confirmpass.html', {
            'action': action,
            'event' : event,
            'opt': opt,
            'thing' : filename,
        })


    def post(self, request, opt, filename):

        if "confirmpass" in request.POST:

            user_pass = request.POST['user_pass']

            reset_pass = config.get('dataset', 'reset_pass')

            if user_pass == reset_pass:

                client_address = get_client_ip(request)

                logging.info('DOWNLOAD : ' + client_address + ' downloaded ' + filename)

                sbj = "Backup Download"
                msg = sbj + '\n\nFile downloaded : ' + filename + '\nIP address : ' + client_address + "\n\nThis mail sent to : ebasar"

                if HOSTNAME[:9] == "applejack":
                    msg2 = msg + ", admin"
                    send_mail(sbj, msg2, "admin")
                    send_mail(sbj, msg2, "ebasar")
                else:
                    send_mail(sbj, msg, "ebasar")

                result = 'Following file is preparing to download : '

                return render(request, 'confirmpass.html', {
                    'opt' : opt,
                    'thing' : filename,
                    'confirmed' : True,
                    'result' : result,
                })

            else:

                action = 'download'
                event = 'download '
                denied_msg = "Wrong password. Please try again."

                return render(request, 'confirmpass.html', {
                    'action': action,
                    'event' : event,
                    'thing' : filename,
                    'confirmed' : False,
                    'denied_msg' : denied_msg,
                })


class Upload(View):

    def get(self, request):

        collectionlist = requests.get(settings.APIURL + 'dbinfo/relevancerdatasets').json()['collections']

        return render(request, 'getfilter.html', {
            'collectionlist' : collectionlist,
        })

    def post(self, request):

        collectionlist = requests.get(settings.APIURL + 'dbinfo/relevancerdatasets').json()['collections']

        if "confirmpass" in request.POST:

            user_pass = request.POST['user_pass']

            reset_pass = config.get('dataset', 'reset_pass')

            if user_pass == reset_pass:

                return render(request, 'getfilter.html', {
                    'collectionlist' : collectionlist,
                    'confirmed' : True,
                })

            else:

                return render(request, 'getfilter.html', {
                    'collectionlist' : collectionlist,
                    'confirmed' : False,
                    'denied_msg' : "Wrong password. Please try again.",
                })

        elif "uploaddataset" in request.POST:

            upfile = request.FILES['upfile']

            keywords = request.POST.get('keywords', '')

            keywordlist = []
            if keywords:
                keywordlist = keywords.split(',')

            if not os.path.isfile('media/' + upfile.name):

                fs = FileSystemStorage()
                filename = fs.save(upfile.name, upfile)
                uploaded_file_url = fs.url(filename)

                p = Process(target=jtp.call_converter, args=(uploaded_file_url[1:], keywordlist, settings.APIURL))
                p.start()

                client_address = get_client_ip(request)

                logging.info('UPLOAD : ' + client_address + ' uploaded ' + filename)

                sbj = "File Uploaded"
                msg = sbj + '\n\nFile uploaded : ' + filename + '\nIP address : ' + client_address + "\n\nThis mail sent to : ebasar"

                if HOSTNAME[:9] == "applejack":
                    msg2 = msg + ", admin"
                    send_mail(sbj, msg2, "admin")
                    send_mail(sbj, msg2, "ebasar")
                else:
                    send_mail(sbj, msg, "ebasar")

                return render(request, 'getfilter.html', {
                    'collectionlist' : collectionlist,
                    'keywordlist':type(keywordlist),
                    'filename': filename,
                })

            else:
                return render(request, 'getfilter.html', {
                    'collectionlist' : collectionlist,
                    'warning': True,
                })

class ResetLabels(View):

    def get(self, request, collname):

        action = 'resetlabels'
        event = 'reset all labels for'

        return render(request, 'confirmpass.html', {
            'action': action,
            'event' : event,
            'thing' : collname,
        })


    def post(self, request, collname):

        if "confirmpass" in request.POST:

            user_pass = request.POST['user_pass']

            reset_pass = config.get('dataset', 'reset_pass')

            if user_pass == reset_pass:

                client_address = get_client_ip(request)

                logging.info('RESET : ' + client_address + ' requested to reset all labels on '   + collname )

                backup_json(collname)

                reset_req = requests.get(settings.APIURL + 'remove/relevancerclusters/'+ collname + '/field/label')

                logging.info('RESET : Reset done for all labels on ' + collname + ', by ' + client_address)

                sbj = "Reset All Labels"
                msg = sbj + '\n\nCollection : ' + collname + '\nIP address : ' + client_address + "\n\nThis mail sent to : ebasar"
                # print ("msg : ", msg)

                if HOSTNAME[:9] == "applejack":
                    msg2 = msg + ", admin"
                    send_mail(sbj, msg2, "admin")
                    send_mail(sbj, msg2, "ebasar")
                else:
                    send_mail(sbj, msg, "ebasar")

                confirmed = True

                result = 'All labels are successfully removed from '

                return render(request, 'confirmpass.html', {
                    'thing' : collname,
                    'confirmed' : confirmed,
                    'result' : result,
                })

            else:

                confirmed = False
                action = 'resetlabels'
                event = 'reset all labels for '
                denied_msg = "Wrong password. Please try again."

                return render(request, 'confirmpass.html', {
                    'action': action,
                    'event' : event,
                    'thing' : collname,
                    'confirmed' : confirmed,
                    'denied_msg' : denied_msg,
                })

        else:
            print("Else part is working!!!!")



class Labeling(View):


    def get(self, request, collname, is_labeled):

        action = 'labeling'
        event = 'see the clusters for '

        return render(request, 'confirmpass.html', {
            'action': action,
            'event' : event,
            'thing' : collname,
            'is_labeled' : is_labeled,
        })


    def post(self, request, collname, is_labeled):

        if "confirmpass" in request.POST:

            user_pass = request.POST['user_pass']

            reset_pass = config.get('dataset', 'reset_pass')

            if user_pass == reset_pass:

                #random_cluster, top10, last10, current_label, warning = get_randomcluster(collname, is_labeled)

                clreq = requests.get(settings.APIURL + "getcluster/relevancerclusters/"+ collname + "/labeled:" + is_labeled).json()

                if(len(clreq)==1):

                    warning = clreq['warning']

                    return render(request, 'label.html', {
                        'warning' : warning,
                    })


                else:

                    random_cluster = clreq['cluster']

                    top10 = clreq['top10']

                    last10 = clreq['last10']

                    if is_labeled=='True':
                        current_label = random_cluster['label']
                    else:
                        current_label = None

                    labellist = requests.get(settings.APIURL + "getsummary/relevancerclusters/"+ collname + "/label/num_tweets").json()['tweets']

                    return render(request, 'label.html', {
                        'random_cluster' : random_cluster,
                        'top10' : top10,
                        'last10' : last10,
                        'labellist' : labellist,
                        'collname' : collname,
                        'is_labeled': is_labeled,
                        'current_label' : current_label,
                    })

            else:

                confirmed = False
                action = "labeling"
                event = 'see the clusters for '
                denied_msg = "Wrong password. Please try again."

                return render(request, 'confirmpass.html', {
                    'action': action,
                    'event' : event,
                    'thing' : collname,
                    'confirmed' : confirmed,
                    'denied_msg' : denied_msg,
                    'is_labeled' : is_labeled,
                })


        elif "labeling" in request.POST:

            client_address = get_client_ip(request)

            #Add the label to DB
            input_label = request.POST['label']
            cl_id = request.POST['cl_id']

            if(input_label==""):

                updatereq = requests.get(settings.APIURL + 'remove/relevancerclusters/' + collname + '/label/' + cl_id)

                logging.info('LABEL: Label deleted from ' + cl_id + ', by ' + client_address + ' for ' + collname)

            else:

                updatereq = requests.get(settings.APIURL + 'update/relevancerclusters/' + collname + '/label/' + input_label + '/' + cl_id)

                logging.info('LABEL: ' + cl_id + ' labeled as "' + input_label + '", by ' + client_address + ' for ' + collname)

            # Request a new random cluster

            clreq = requests.get(settings.APIURL + "getcluster/relevancerclusters/"+ collname + "/labeled:" + is_labeled).json()

            if(len(clreq)==1):

                warning = clreq['warning']

                return render(request, 'label.html', {
                    'warning' : warning,
                })


            else:

                random_cluster = clreq['cluster']

                top10 = clreq['top10']

                last10 = clreq['last10']

                if is_labeled=='True':
                    current_label = random_cluster['label']
                else:
                    current_label = None

                labellist = requests.get(settings.APIURL + "getsummary/relevancerclusters/"+ collname + "/label/num_tweets").json()['tweets']

                return render(request, 'label.html', {
                    'random_cluster' : random_cluster,
                    'top10' : top10,
                    'last10' : last10,
                    'labellist' : labellist,
                    'collname' : collname,
                    'is_labeled': is_labeled,
                    'current_label' : current_label,
                })


        elif "directlabel" in request.POST:

            client_address = get_client_ip(request)

            #Add the label to DB
            input_label = request.POST['directlabel'].strip()
            cl_id = request.POST['cl_id']

            updatereq = requests.get(settings.APIURL + 'update/relevancerclusters/' + collname + '/label/' + input_label + '/' + cl_id)

            logging.info('LABEL: ' + cl_id + ' labeled as "' + input_label + '", by ' + client_address + ' for ' + collname)

            # Request a new random cluster

            clreq = requests.get(settings.APIURL + "getcluster/relevancerclusters/"+ collname + "/labeled:" + is_labeled).json()

            if(len(clreq)==1):

                warning = clreq['warning']

                return render(request, 'label.html', {
                    'warning' : warning,
                })

            else:

                random_cluster = clreq['cluster']

                top10 = clreq['top10']

                last10 = clreq['last10']

                if(is_labeled=='True'):
                    current_label = random_cluster['label']
                else:
                    current_label = None

                labellist = requests.get(settings.APIURL + "getsummary/relevancerclusters/"+ collname + "/label/num_tweets").json()['tweets']

                return render(request, 'label.html', {
                    'random_cluster' : random_cluster,
                    'top10' : top10,
                    'last10' : last10,
                    'labellist' : labellist,
                    'collname' : collname,
                    'is_labeled': is_labeled,
                    'current_label' : current_label,
                })

        elif "nextcl" in request.POST:

            clreq = requests.get(settings.APIURL + "getcluster/relevancerclusters/" + collname + "/labeled:" + is_labeled).json()

            random_cluster = clreq['cluster']

            top10 = clreq['top10']

            last10 = clreq['last10']

            if is_labeled == 'True':
                current_label = random_cluster['label']
            else:
                current_label = None

            labellist = requests.get(settings.APIURL + "getsummary/relevancerclusters/"+ collname + "/label/num_tweets").json()['tweets']

            return render(request, 'label.html', {
                'random_cluster' : random_cluster,
                'top10' : top10,
                'last10' : last10,
                'labellist' : labellist,
                'collname' : collname,
                'is_labeled': is_labeled,
                'current_label' : current_label,
            })


class GetFilter(View):

    def get(self, request):

        req = requests.get(settings.APIURL + 'dbinfo/relevancerdatasets')

        collectionlist = req.json()['collections']

        return render(request, 'getfilter.html', {
            'collectionlist' : collectionlist,
        })



class GetFilterSteps(View):

    def get(self, request, collname):

        action = 'filter'
        event = 'analyse the collection named '

        return render(request, 'confirmpass_filter.html', {
            'action': action,
            'event' : event,
            'thing' : collname,
        })



    def post(self, request, collname):

        if "confirmpass" in request.POST:

            client_address = get_client_ip(request)

            user_pass = request.POST['user_pass']

            filter_pass = config.get('filtercolls', 'filterpass')

            if(user_pass == filter_pass):

                logging.info(client_address + ' started creating a query for ' + collname)

                keywordlist = requests.get(settings.APIURL + 'getsummary/relevancerdatasets/'+ collname + '/field/keywords').json()['field']

                model_filters= requests.get(settings.APIURL + 'getdata/relevancer/QueryList/field/filtername').json()['value_list']

                return render(request, 'getfiltersteps.html', {
                    'filtername':True,
                    'collname':collname,
                    'keywordlist':keywordlist,
                    'model_filters':model_filters,
                })

            else:

                confirmed = False
                action = "filter"
                event = 'analyse the collection named '
                denied_msg = "Wrong password. Please try again."

                return render(request, 'confirmpass_filter.html', {
                    'action': action,
                    'event' : event,
                    'thing' : collname,
                    'confirmed' : confirmed,
                    'denied_msg' : denied_msg,
                })


        elif "users-skip" in request.POST:

            username = request.POST['username']
            usersurname = request.POST['usersurname']
            useremail = request.POST['useremail']

            filtername = request.POST['filtername']

            keyword = request.POST.getlist('keywordlist')[0]

            keywordlist = requests.get(settings.APIURL + 'getsummary/relevancerdatasets/'+ collname + '/field/keywords').json()['field']

            model_filters = requests.get(settings.APIURL + 'getdata/relevancer/QueryList/field/filtername').json()['value_list']

            if "_" in filtername:

                return render(request, 'getfiltersteps.html', {
                    'filtername':True,
                    'collname':collname,
                    'denied_msg': 'You cannot use underscore in the filtername.',
                    'keywordlist':keywordlist,
                    'model_filters':model_filters,
                })


            elif filtername in model_filters:


                return render(request, 'getfiltersteps.html', {
                    'filtername':True,
                    'collname':collname,
                    'denied_msg': 'This filter name is in use. Please write another.',
                    'keywordlist':keywordlist,
                    'model_filters':model_filters,
                })

            else:

                querydata = {'collname':collname, 'filtername':filtername, 'username':username, 'usersurname':usersurname, 'useremail':useremail, 'keyword':keyword}

                qid = requests.post(settings.APIURL + 'insert/relevancer/QueryList', json=json.dumps({'documents':querydata})).json()['objid']

                ############# The Tweets and Users with Urls

                opt_u = 'urls'

                user_dict_url = requests.post(settings.APIURL + 'mostfrequent/relevancerdatasets/'+ collname +'/usernames', json=json.dumps({'n':'10','opt':opt_u,'keyword': keyword})).json()['userlist']

                numtwt_urls = requests.post(settings.APIURL + 'gettweetids/relevancerdatasets/'+ collname, json=json.dumps({'objid':qid,'opt':opt_u,'keyword': keyword})).json()['numberoftweets']

                #############  The Tweets and Users with Photos

                opt_p = 'photos'

                user_dict_photos = requests.post(settings.APIURL + 'mostfrequent/relevancerdatasets/'+ collname +'/usernames', json=json.dumps({'n':'10','opt':opt_p,'keyword': keyword})).json()['userlist']

                numtwt_photos = requests.post(settings.APIURL + 'gettweetids/relevancerdatasets/'+ collname, json=json.dumps({'objid':qid,'opt':opt_p,'keyword': keyword})).json()['numberoftweets']

                ############ The Tweets and Users without any Photos or Urls

                opt_no = 'nophotosurls'

                user_dict_no = requests.post(settings.APIURL + 'mostfrequent/relevancerdatasets/'+ collname +'/usernames', json=json.dumps({'n':'10','opt':opt_no,'keyword': keyword})).json()['userlist']

                numtwt_nophotosurls = requests.post(settings.APIURL + 'gettweetids/relevancerdatasets/'+ collname, json=json.dumps({'objid':qid,'opt':opt_no,'keyword': keyword})).json()['numberoftweets']


                return render(request, 'getfiltersteps.html', {
                    #'dbbackend' : get_dbbackend(collname),
                    'step' : 'users-skip',
                    'collname' : collname,
                    'queryid' : qid,
                    'numtwt_urls' : numtwt_urls,
                    'numtwt_photos' : numtwt_photos,
                    'numtwt_nophotosurls':numtwt_nophotosurls,
                    'users_urls':user_dict_url,
                    'users_photos':user_dict_photos,
                    'users_nophotosurls':user_dict_no,
                })



        elif "hashtags-skip" in request.POST:

            userlist_urls = request.POST.getlist('userlist_urls')
            userlist_photos = request.POST.getlist('userlist_photos')
            userlist_nophotosurls = request.POST.getlist('userlist_nophotosurls')

            queryid = request.POST['queryid']

            updatepayload = {'objid':queryid, 'userlist_urls':userlist_urls, 'userlist_photos':userlist_photos,'userlist_nophotosurls':userlist_nophotosurls}

            qupdatereq = requests.post(settings.APIURL + 'update/relevancer/QueryList', json=json.dumps(updatepayload))


            ############# The Tweets and Users with Urls

            opt_u = 'urls'

            hashreq_u = requests.post(settings.APIURL + 'mostfrequent/relevancerdatasets/'+ collname +'/hashtags', json=json.dumps({'objid':queryid,'n':'10','opt':opt_u})).json()

            hashtag_list_url = hashreq_u['hashtaglist']

            numtwt_urls = hashreq_u['numberoftweets']

            #############  The Tweets and Users with Photos

            opt_p = 'photos'

            hashreq_p = requests.post(settings.APIURL + 'mostfrequent/relevancerdatasets/'+ collname +'/hashtags', json=json.dumps({'objid':queryid,'n':'10','opt':opt_p})).json()

            hashtag_list_photos = hashreq_p['hashtaglist']

            numtwt_photos = hashreq_p['numberoftweets']

            ############ The Tweets and Users without any Photos or Urls

            opt_no = 'nophotosurls'

            hashreq_no = requests.post(settings.APIURL + 'mostfrequent/relevancerdatasets/'+ collname +'/hashtags', json=json.dumps({'objid':queryid,'n':'10','opt':opt_no})).json()

            hashtag_list_no = hashreq_no['hashtaglist']

            numtwt_nophotosurls = hashreq_no['numberoftweets']


            return render(request, 'getfiltersteps.html', {
                #'dbbackend' : get_dbbackend(collname),
                'step' : 'hashtags-skip',
                'collname' : collname,
                'queryid' : queryid,
                'numtwt_urls' : numtwt_urls,
                'numtwt_photos' : numtwt_photos,
                'numtwt_nophotosurls':numtwt_nophotosurls,
                'hashtags_urls':hashtag_list_url,
                'hashtags_photos':hashtag_list_photos,
                'hashtags_nophotosurls':hashtag_list_no,

            })


        elif "overview-skip" in request.POST:

            hashtaglist_urls = request.POST.getlist('hashtaglist_urls')
            hashtaglist_photos = request.POST.getlist('hashtaglist_photos')
            hashtaglist_nophotosurls = request.POST.getlist('hashtaglist_nophotosurls')

            queryid = request.POST['queryid']

            updatepayload = {'objid':queryid, 'hashtaglist_urls':hashtaglist_urls, 'hashtaglist_photos':hashtaglist_photos,'hashtaglist_nophotosurls':hashtaglist_nophotosurls}

            qupdatereq = requests.post(settings.APIURL + 'update/relevancer/QueryList', json=json.dumps(updatepayload))


            ############# The Tweets and Users with Urls

            opt_u = 'urls'

            numtwt_urls = requests.post(settings.APIURL + 'exclude/relevancerdatasets/'+ collname +'/hashtags', json=json.dumps({'objid':queryid,'opt':opt_u})).json()['numberoftweets']

            #############  The Tweets and Users with Photos

            opt_p = 'photos'

            numtwt_photos  = requests.post(settings.APIURL + 'exclude/relevancerdatasets/'+ collname +'/hashtags', json=json.dumps({'objid':queryid,'opt':opt_p})).json()['numberoftweets']

            ############ The Tweets and Users without any Photos or Urls

            opt_no = 'nophotosurls'

            numtwt_nophotosurls = requests.post(settings.APIURL + 'exclude/relevancerdatasets/'+ collname +'/hashtags', json=json.dumps({'objid':queryid,'opt':opt_no})).json()['numberoftweets']

            ############ Get the last version of the query

            query = requests.get(settings.APIURL + 'getdata/relevancer/QueryList/objectid/' + queryid).json()

            return render(request, 'getfiltersteps.html', {
                #'dbbackend' : get_dbbackend(collname),
                'step' : 'overview-skip',
                'collname' : collname,
                'queryid' : queryid,
                'query':query,
                'numtwt_urls' : numtwt_urls,
                'numtwt_photos' : numtwt_photos,
                'numtwt_nophotosurls':numtwt_nophotosurls,
                'numtwt_total':  int(numtwt_urls) +  int(numtwt_photos) +int(numtwt_nophotosurls),
            })


        elif "cluster" in request.POST:

            queryid = request.POST['queryid']

            query = requests.get(settings.APIURL + 'getdata/relevancer/QueryList/objectid/' + queryid).json()

            p = Process(target=drc.auto_cluster, args=(collname, queryid, settings.APIURL))
            p.start()

            return render(request, 'getfiltersteps.html', {
                #'dbbackend' : get_dbbackend(collname),
                'step' : 'clustering',
                'collname' : collname,
                'query' : query,
            })

class HowItWorks(View):

    def get(self, request, page):

        if page=="Introduction":
            intro = "True"
            tweets = ""
            length = ""
            current_page = ""
            nextpage = ""
            next_step = ""

        if page == "Raw_Data":
            intro = "False"
            tweets, length = get_step_data("testcl", 500)
            current_page = "Raw Data"
            nextpage = "Eliminate_Retweets"
            next_step = "Eliminate Retweets"

        elif page == "Eliminate_Retweets":
            intro = "False"
            tweets, length = get_step_data("rt_eliminated", 500)
            current_page = "Retweets are Eliminated"
            nextpage = "Remove_Duplicates"
            next_step = "Remove Duplicates"

        elif page == "Remove_Duplicates":
            intro = "False"
            tweets, length = get_step_data("duplicates_eliminated", 500)
            current_page = "Duplicate Tweets are Eliminated"
            nextpage = "Cluster_Them"
            next_step = "Cluster Them"

        elif page == "Cluster_Them":
            intro = "False"
            tweets, length = get_step_data("genocide_clusters_20151005", 10, "Cluster_Them")
            current_page = "Tweets are Clustered"
            nextpage = "Label_the_Clusters"
            next_step = "Label the Clusters"

        elif page == "Label_the_Clusters":

            return HttpResponseRedirect('/datasets')

        return render(request, 'howitworks.html', {
            'intro':intro,
            'tweets':tweets,
            'length':length,
            'current_page': current_page,
            'nextpage': nextpage,
            'next_step': next_step,
        })



class About(View):

    def get(self, request):

        return render(request, 'about.html', {
        })

class DjangoCalendar(View):


    def get(self, request):


        return render(request, 'query.html', {
        })

    def post(self, request):

        from_date = request.POST['from']
        to_date = request.POST['to']
        from_hour = request.POST['hours_from']
        to_hour = request.POST['hours_to']
        keyterm = request.POST['keyterm']

        casesensitive = request.POST.getlist('casesensitive')

        # print ("*****from_date : " + from_date)
        # print ("*****to_date : " + to_date)
        # print ("*****from_hour : " + from_hour)
        # print ("*****to_hour : " + to_hour)
        # print ("queryyyyyy : " + keyterm)
        # print (casesensitive)

        if len(casesensitive)==0:
            casesensitive_link="casesensitive=false"
        else :
            casesensitive_link="casesensitive=true"

        query_link="query="+keyterm

        start_time="start="+from_date+"T"+from_hour+":00.000Z"
        end_time="end="+to_date+"T"+to_hour+":00.000Z"

        # from_date=from_date.split("-")
        # from_hour=from_hour.split(":")

        # start_time = datetime(int(from_date[2]),int(from_date[1]),int(from_date[0]),int(from_hour[0]),int(from_hour[1]))
        # print (start_time)

        # to_date=to_date.split("-")
        # to_hour=to_hour.split(":")
        # end_time = datetime(int(to_date[2]),int(to_date[1]),int(to_date[0]),int(to_hour[0]),int(to_hour[1]))
        # print (end_time)

        user_name = config.get('twiqsnl_settings', 'user_name')
        password = config.get('twiqsnl_settings', 'password')


        #start_time = datetime(2014,6,1)
        #end_time = datetime(2015,1,1)


        # hour_str_list = []
        # while end_time > start_time:

        #     start_time += timedelta(hours=1)
        #     hour_url_str = start_time.strftime("year=%Y&month=%m&day=%d&hour=%H&minute=%M")
        #     hour_str_list.append(hour_url_str)

        #print ("hour_url_str : " + hour_url_str)

        #base_url = "https://"+user_name + ":" + password + "@twinl.surfsara.nl/api/search/get_hour?"
        base_url = "https://"+user_name + ":" + password + "@twinl.surfsara.nl/api/search/tweets_ids?"
        print("base_url : " + base_url)

        #i = 0
        #for h_url in hour_str_list:

        tweethoururl = base_url+start_time +"&"+end_time + "&" +query_link +"&"+ casesensitive_link +"&"+ "startfrom=0&size=10"
        print(tweethoururl)
        a="https://lstru:pZJaaJOE5JhMvIOsiY@twinl.surfsara.nl/api/search/tweets_ids?start=2016-06-11T00:00:00.000Z&end=2016-06-11T13:20:00.000Z&query=ugur&casesensitive=false&startfrom=0&size=10"
        heatmap ="https://lstru:pZJaaJOE5JhMvIOsiY@twinl.surfsara.nl/api/search/geo?start=2016-06-11T00:00:00.000Z&end=2016-07-11T13:20:00.000Z&query=eten&casesensitive=false"

        print(a==tweethoururl)
        #tweethoururl=requests.get(tweethoururl)
        #heatmap=requests.get(heatmap)
        tweethoururl=requests.get("https://lstru:pZJaaJOE5JhMvIOsiY@twinl.surfsara.nl/api/search/users?start=2016-06-11T00:00:00.000Z&end=2016-07-11T13:20:00.000Z&query=eten&casesensitive=false")

        ugur = tweethoururl.json()

        #tweethoururl = base_url+h_url.replace("&","\&")
        #tweethoururl ="https://lstru:pZJaaJOE5JhMvIOsiY@twinl.surfsara.nl/api/search/tweets_ids?start=2016-06-11T00:00:00.000Z&end=2016-07-11T13:20:00.000Z&query=eten&casesensitive=false&startfrom=0&size=10"

        ugur2=len(tweethoururl.json())
        print(tweethoururl)

        return render(request, 'query.html', {
                'positive_msg' : " Ugur make correct !!",
                'ugur' : ugur,
                'ugur2' : ugur2,

            })
