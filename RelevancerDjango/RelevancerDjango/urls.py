from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static

from django.contrib import admin
admin.autodiscover()


from main.views import *

urlpatterns = patterns('',

    url(r'^$', Home.as_view(), name='home'),

    url(r'^dashboard/', include('dashboardapp.urls')),

    url(r'^how_does_it_work/step:(?P<page>.+)$', HowItWorks.as_view(), name='howdoesitwork'),

    url(r'^datasets$', Datasets.as_view(), name='datasets'),

    url(r'^datasets/(?P<collname>.+)/backup$', Backup.as_view(), name='backup'),

    url(r'^datasets/(?P<collname>.+)/listbackups$', ListBackups.as_view(), name='listbackups'),

    url(r'^datasets/(?P<collname>.+)/listclassifiers$', ListClassifiers.as_view(), name='listclassifiers'),

    url(r'^datasets/(?P<collname>.+)/evaluationreport$', EvaluationReport.as_view(), name='evaluationreport'),

    url(r'^datasets/(?P<filename>.+)/evaluationyourself$', EvaluationYourself.as_view(), name='evaluationyourself'),

    url(r'^datasets/(?P<filename>.+)/loadback$', LoadBack.as_view(), name='loadback'),

    url(r'^datasets/(?P<collname>.+)/generate_classifier$', Classifier.as_view(), name='classifier'),

    url(r'^datasets/(?P<opt>.+)/(?P<filename>.+)/download$', Download.as_view(), name='download'),

    url(r'^datasets/(?P<opt>.+)/(?P<filename>.+)/downloadfile$', downloadfile, name='downloadfile'),

    url(r'^upload$', Upload.as_view(), name='upload'),

    url(r'^datasets/(?P<opt>.+)/(?P<filename>.+)/displayfile$', downloadfile, name='displayfile'),

    url(r'^datasets/(?P<collname>.+)/labeled:(?P<is_labeled>.+)$', Labeling.as_view(), name='labeling'),

    url(r'^datasets/(?P<collname>.+)/reset_labels$', ResetLabels.as_view(), name='resetlabels'),

    url(r'^getfilter$', GetFilter.as_view(), name='getfilter'),

    url(r'^getfilter/data:(?P<collname>.+)$', GetFilterSteps.as_view(), name='getfiltersteps'),

    url(r'^about$', About.as_view(), name='about'),

    url(r'^query$', DjangoCalendar.as_view(), name='query'),

)

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
