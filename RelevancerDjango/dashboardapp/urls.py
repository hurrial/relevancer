from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static

from django.contrib import admin
admin.autodiscover()


from dashboardapp.views import *

urlpatterns = patterns('',

	url(r'^$', DashHome.as_view(), name='dashhome'),

)

