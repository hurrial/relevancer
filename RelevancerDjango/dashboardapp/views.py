from django.shortcuts import render
from django.views.generic import View

import os
import json
import requests



# RelevancerAPI URL
APIURL = 'http://127.0.0.1:5006/'

timeformat = '%d-%m-%Y,%H:%M'

# Create the necessary folders 
os.makedirs("static/dashboard/data/"	, exist_ok=True)
os.makedirs("static/dashboard/data/dcdata/"	, exist_ok=True)
os.makedirs("static/dashboard/data/overview/"	, exist_ok=True)


def get_crossfilter_data(untilstr, sincestr, datasetname, term='', hasLocation='False'):

	queryid = sincestr + '&' + untilstr + '&' + datasetname + '&' + term + '&' + hasLocation

	req = requests.get(APIURL + 'getdata/relevancerdatasets/' + datasetname +'/perinterval/since=' + sincestr + '&until='+untilstr + '&hasLocation=' + hasLocation)

	totaltweets = req.json()['documents']

	filepath = 'dashboard/data/dcdata/' + queryid + '.json'

	with open('static/' + filepath, 'w') as f:
		json.dump(totaltweets, f)

	return filepath


def get_datelist(untilstr, sincestr, datasetname, term='', hasLocation='False', skip='1000'):

	datelist = requests.get(APIURL + 'getdatelist/relevancerdatasets/' + datasetname +'/perinterval/since=' + sincestr + '&until='+untilstr + '&hasLocation=' + hasLocation + '&skip='+skip).json()['dates']

	firstdate = datelist.copy()
	seconddate = datelist.copy()
	del firstdate[-1]
	del seconddate[0]

	zippeddates = [{'firstdate': t[0], 'seconddate': t[1]} for t in zip(firstdate, seconddate)]

	return zippeddates


def get_graph_data(untilstr, sincestr, datasetname, term='', hasLocation='False', entries='50'):

	overview_filepath = 'dashboard/data/overview/overview_' + sincestr + '&' + untilstr + '&' + datasetname + '&' + term + '&' + hasLocation + '.json'

	overviewdata = requests.get(APIURL + 'getoverviewdata/relevancerdatasets/' + datasetname +'/perinterval/since=' + sincestr + '&until='+untilstr + '&hasLocation=' + hasLocation + '&entries='+entries).json()['overviewdata']
	
	with open('static/' + overview_filepath, 'w') as f:
		json.dump(overviewdata, f)

	return overview_filepath


class DashHome(View):

	def get(self, request):

		collectionlist = requests.get(APIURL + 'dbinfo/relevancerdatasets').json()['collections']
		
		return render(request, 'dashboard.html', {
			'collectionlist': collectionlist,
			'dcfilepath': False,
		})

	def post(self, request):

		if "analyze" in request.POST:

			sincestr = request.POST['since']
			untilstr = request.POST['until']
			datasetname = request.POST['datasetname']
			term = request.POST['term']
			hasLocation = request.POST.getlist('hasLocation')

			if (hasLocation):
				hasLocation = 'True'
			else:
				hasLocation = 'False'

			datelistzip = get_datelist(untilstr, sincestr, datasetname, term, hasLocation, '1000')

			newuntilstr = datelistzip[0]['seconddate']
			newsincestr = datelistzip[0]['firstdate']

			dcfilepath = get_crossfilter_data(newuntilstr, newsincestr, datasetname, term, hasLocation)
			
			if(len(datelistzip) > 1):
				overview_filepath = get_graph_data(untilstr, sincestr, datasetname, term, hasLocation, '50')
			else:
				overview_filepath = False

			collectionlist = requests.get(APIURL + 'dbinfo/relevancerdatasets').json()['collections']

			return render(request, 'dashboard.html', {
					'collectionlist': collectionlist,
					'datasetname':datasetname,
					'since':sincestr,
					'until':untilstr,
					'newsince':newsincestr,
					'newuntil':newuntilstr,
					'datelistzip':datelistzip,
					'dcfilepath': dcfilepath,
					'overview_filepath': overview_filepath,
					'term': term,
					'hasLocation':hasLocation,
			})


		elif "analyzefurther" in request.POST:

			sincestr = request.POST['since']
			untilstr = request.POST['until']
			term = request.POST['term']
			datasetname = request.POST['datasetnameft']
			hasLocation = request.POST['hasLocationft']

			newdates = request.POST.getlist('newdates')[0].split('|')

			newsincestr = newdates[0]
			newuntilstr = newdates[1]

			datelistzip = get_datelist(untilstr, sincestr, datasetname, term, hasLocation, '1000')

			dcfilepath = get_crossfilter_data(newuntilstr, newsincestr, datasetname, term, hasLocation)
			
			overview_filepath = get_graph_data(untilstr, sincestr, datasetname, term, hasLocation, '50')

			collectionlist = requests.get(APIURL + 'dbinfo/relevancerdatasets').json()['collections']

			return render(request, 'dashboard.html', {
					'collectionlist': collectionlist,
					'datasetname':datasetname,
					'since':sincestr,
					'until':untilstr,
					'newsince':newsincestr,
					'newuntil':newuntilstr,
					'datelistzip':datelistzip,
					'dcfilepath': dcfilepath,
					'overview_filepath': overview_filepath,
					'term': term,
					'hasLocation':hasLocation,
			})








