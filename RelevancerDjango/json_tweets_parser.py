
################################################
##### 2016/08/24, Florian Kunneman
##### Scripts to convert tweets in json format (from the Twitter API or a different database, like TwiNL) into the format used in the relevancer framework
##### Usage:
### import json_tweets_parser
### jtp = json_tweets_parser.Json_tweets_parser(jsonfile)
### jtp.parse()
### jtp.extract_keywords([my_keywords])
### converted_lines = jtp.convert2relevancer()
###


import re
import json
import datetime
import requests

import docreader

month = {"Jan" : "01", "Feb" : "02", "Mar" : "03", "Apr" : "04", "May" : "05", "Jun" : "06", "Jul" : "07",
    "Aug" : "08", "Sep" : "09", "Oct" : "10", "Nov" : "11", "Dec" : "12"}
date_time = re.compile(r"(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) (\d+) (\d{2}:\d{2}:\d{2}) \+\d+ (\d{4})")


def call_converter(jsonfile, keywordslist, APIURL):

    with open(jsonfile, 'r') as json_file:
        jsondata = json.load(json_file)

    if 'tags' in jsondata:
        # if it is he FloodTags data format already.
        converted_lines = jsondata['tags']
        for tag in converted_lines:
            if 'urls' not in tag['urls'] and 'urls' in tag['source']:
                tag['urls'] = tag['source']['urls']
                del tag['source']['urls']
    else:
        jtp = Json_tweets_parser(jsonfile)
        jtp.parse()
        jtp.extract_keywords(keywordslist)
        converted_lines = jtp.convert2relevancer()

    # Add the collection to database with RelevancerAPI
    collname = jsonfile.split('/')[-1][:-5]

    savereq = requests.post(APIURL + 'insert/relevancerdatasets/' + collname, json=json.dumps({'documents':converted_lines}))

    return  converted_lines


class Json_tweets_parser:

    def __init__(self, jsonfile):
        self.jsonfile = jsonfile
        self.lines = []
        self.columns = ['tweet_id', 'user_id', 'user_name', 'user_followers', 'user_location', 'date', 'time',
            'reply_to_user', 'retweet_to_user', 'tweet_url', 'tweet_text', 'image', 'urls']
        self.column_keys = [['id'], ['user', 'id'], ['user', 'screen_name'], ['user', 'followers_count'],
            ['user', 'location'], ['created_at'], ['in_reply_to_screen_name'], ['retweeted_status', 'user',
            'screen_name'], ['text'], ['entities', 'media', 'list', 'media_url'], ['entities', 'urls', 'list', 'expanded_url'], ['geo', 'coordinates', 'list']]

    def parse(self):
        dr = docreader.Docreader()
        self.lines = dr.parse_json(self.jsonfile, self.column_keys)

    def extract_keywords(self, kw): # assumes lines as formatted from parse
        ### NOTE: Word boundaries are for now excluded, as they did not work during testing
        #regex = r'\b'+r'\b|\b'.join([re.escape(k) for k in kw])+r'\b'
        regex = r'|'.join([re.escape(k) for k in kw])
        #print('REGEX...', regex)
        keyword_match = re.compile(regex, re.IGNORECASE)
        newlines = []
        for line in self.lines:
            text = line[8]
            keywords = keyword_match.findall(text)
            line.append(keywords)
            newlines.append(line)
        self.lines = newlines

    def convert2lists(self):
        newlines = []
        for line in self.lines:
            # set tweet_id and user_id to str
            line[0] = str(line[0])
            line[1] = str(line[1])
            # extract date and time object from 'created at'
            dt = date_time.search(line[5]).groups()
            timefields = [int(f) for f in dt[2].split(':')]
            date = datetime.date(int(dt[3]), int(month[dt[0]]), int(dt[1]))
            time = datetime.time(timefields[0], timefields[1], timefields[2])
            # generate tweet url
            url = 'https://twitter.com/' + line[2] + '/status/' + line[0]
            # assemble new line and add to new lines
            newlines.append(line[:5] + [date, time] + line[6:8] + [url, line[8]])
        return newlines

    def convert2relevancer(self):
        newlines = []
        for line in self.lines:
            relevancer_obj = {}
            photos = []
            if len(line[-4]) > 0:
                for field in line[-3]:
                    if field[-4:] == '.jpg':
                        photos.append(field)
            relevancer_obj['photos'] = photos
            relevancer_obj['urls'] = line[-3]
            if len(line[-2]) > 0:
                relevancer_obj['locations'] = [{'loc' : {'lat': line[-2][0], 'lon': line[-2][1]}}]
            else:
                relevancer_obj['locations'] = []
            relevancer_obj['keywords'] = line[-1]
            relevancer_obj['waterDepth'] = 0
            # transform datetime
            dt = date_time.search(line[5]).groups()
            timefields = [int(f) for f in dt[2].split(':')]
            datetime_obj = datetime.datetime(int(dt[3]), int(month[dt[0]]), int(dt[1]), timefields[0], timefields[1], timefields[2])
            relevancer_obj['date'] = datetime_obj.isoformat()
            relevancer_obj['text'] = line[8]
            relevancer_obj['source'] = {}
            relevancer_obj['source']['id'] = str(line[0])
            relevancer_obj['source']['username'] = line[2]
            relevancer_obj['source']['userId'] = str(line[1])
            newlines.append(relevancer_obj)
        return newlines


if __name__=='__main__':

    keywordlist = ['vaccinatie', '#vaccinatie', 'inenting', '#inenting', 'vaccin', '#vaccin', 'bmr', '#bmr', 'dktp', '#dktp', 'hpv', '#hpv', 'rotavirus', '#rotavirus', 'buikgriep', '#buikgriep', 'waterpokken', '#waterpokken', 'bmrv-vaccin', 'gordelroos', '#gordelroos', 'hepatitis a', 'herpes zoster', 'windpokken', '#windpokken', 'meningkokken b', 'griepprik', '#griepprik', 'griepvaccinatie', '#griepvaccinatie', 'influenza', '#influenza']

    cl = call_converter('media/vaccinationNL.json', keywordlist, 'http://127.0.0.1:5005/')

    #print(cl)
    for c in cl:
        print(c['keywords'])
